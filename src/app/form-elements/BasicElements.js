import React, { Component } from 'react';
import { Form } from 'react-bootstrap';
import DatePicker from "react-datepicker";
import bsCustomFileInput from 'bs-custom-file-input'
import {Line, Bar, Doughnut, Pie, Scatter} from 'react-chartjs-2';

export class BasicElements extends Component {
  state = {
    startDate: new Date()
  };
 
  handleChange = date => {
    this.setState({
      startDate: date
    });
  };
  componentDidMount() {
    bsCustomFileInput.init()
    }
  
    data = {
      labels: ["Mar14", "Mar15", "Mar16", "Mar17", "Mar18", "Mar19"],
      datasets: [{
        label: '# of parcels',
        data: [10, 19, 3, 5, 2, 3],
        
        width:10,
        borderWidth: 1,
        fill: false
      }]
  };

  options = {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      },
      legend: {
        display: false
      },
      elements: {
        point: {
          radius: 0
        }
      }
  
  };
  destinationData = {
    labels: ["Mbarara", "Arua", "Masaka", "Kabale", "Mubende", "Lira"],
    datasets: [{
      label: '# of parcels',
      data: [10, 19, 3, 5, 2, 3], 
      
      width:10,
      borderWidth: 1,
      fill: false
    }]
};

destinationOptions = {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    },
    legend: {
      display: false
    },
    elements: {
      point: {
        radius: 0
      }
    }

};

multiData = {
  
  datasets: [
    {
      label: 'Dataset 1',
      data: [10, 19, 15, 5, 2, 3],
      fill: false,
      borderColor: 'rgb(255, 99, 132)',
    },
    {
      label: 'Dataset 2',
      data: [20, 29, 13, 6, 7, 5],
      fill: false,
      borderColor: 'rgb(53, 162, 235)',
    },
  ],
  
};
multiOptions = {
  responsive: true,
  type: 'linear',
  scales: {
    yAxes: [{
      ticks: {
        beginAtZero: true
      }
    }]
  },
  legend: {
    display: false
  },
  elements: {
    point: {
      radius: 0
    }
  }

};

  areaData = {
    labels: ["March", "April", "May", "June", "July", "August"],
    datasets: [{
        label: 'UGX',
        data: [1200000, 1900000, 3000000, 5000000, 2000000, 3000000],
        
        borderColor: [
          'black',
          
        ],
        borderWidth: 1,
        fill: true, // 3: no fill
      }]
  };

  areaOptions = {
      plugins: {
        filler: {
          propagate: true
        }
      }
  }

  doughnutPieData = {
      datasets: [{
        data: [30, 40, 30],
        backgroundColor: [
          'rgba(255, 99, 132, 0.5)',
          'rgba(54, 162, 235, 0.5)',
          'rgba(255, 206, 86, 0.5)',
          'rgba(75, 192, 192, 0.5)',
          'rgba(153, 102, 255, 0.5)',
          'rgba(255, 159, 64, 0.5)'
        ],
        borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
        ],
      }],
  
      // These labels appear in the legend and in the tooltips when hovering different arcs
      labels: [
        'Pink',
        'Blue',
        'Yellow',
      ]
  };

  doughnutPieOptions = {
      responsive: true,
      animation: {
        animateScale: true,
        animateRotate: true
      }
  };

  scatterChartData = {
      datasets: [{
        label: 'First Dataset',
        data: [{
          x: -10,
          y: 0
        },
        {
          x: 0,
          y: 3
        },
        {
          x: -25,
          y: 5
        },
        {
          x: 40,
          y: 5
        }
        ],
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)'
        ],
        borderColor: [
          'rgba(255,99,132,1)'
        ],
        borderWidth: 1
      },
      {
        label: 'Second Dataset',
        data: [{
          x: 10,
          y: 5
        },
        {
          x: 20,
          y: -30
        },
        {
          x: -25,
          y: 15
        },
        {
          x: -10,
          y: 5
        }
        ],
        backgroundColor: [
          'rgba(54, 162, 235, 0.2)',
        ],
        borderColor: [
          'rgba(54, 162, 235, 1)',
        ],
        borderWidth: 1
      }
      ]
  }
  pageViewAnalyticData = {
    labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28"],
    datasets: [{
        label: 'Delivered',
        data: [46, 49, 51, 58, 63.61, 65, 64, 69, 70, 78, 80, 80, 90, 85, 87, 92, 97, 102, 107, 109, 111, 111, 120, 130, 132, 136, 140, 145],
        backgroundColor: [
          'rgba(216,247,234, 0.19)',
        ],
        borderColor: [
          '#3dd597'
        ],
        borderWidth: 2,
        fill: true,
        pointBorderColor: "#fff",
        pointBackgroundColor: "#3dd597",
        pointBorderWidth: 2,
        pointRadius: 4,
      },
      {
        label: 'Canceled',
        data: [16, 19, 21, 28, 33.31, 35, 34, 39, 40, 48, 50, 50, 51, 55, 57, 62, 67, 69, 68, 70, 72, 75, 74, 80, 79, 80, 84, 90],
        backgroundColor: [
          'red',
        ],
        borderColor: [
          'red'
        ],
        borderWidth: 2,
        fill: false,
        pointBorderColor: "#fff",
        pointBackgroundColor: "#0162ff",
        pointBorderWidth: 2,
        pointRadius: 4,
      }
    ],
  };
  pageViewAnalyticOptions = {
    scales: {
      yAxes: [{
        display: true,
        gridLines: {
          drawBorder: false,
          display: true,
          drawTicks: false,
          color: '#eef0fa',
          zeroLineColor: 'rgba(90, 113, 208, 0)',
        },
        ticks: {
          beginAtZero: true,
          stepSize: 50,
          display: true,
          padding: 10,
        }
      }],
      xAxes: [{
        display: true,
        position: 'bottom',
        gridLines: {
          drawBorder: false,
          display: false,
          drawTicks: false,
        },
        ticks: {
          beginAtZero: true,
          stepSize: 10,
          fontColor: "#a7afb7",
          padding: 10,
        }
      }],
    },
    legend: {
      display: false,
    },
    elements: {
      point: {
        radius: 1
      },
      line: {
        tension: 0
      }
    },
    tooltips: {
      backgroundColor: 'rgba(2, 171, 254, 1)',
    },
  };
  scatterChartOptions = {
      scales: {
        xAxes: [{
          type: 'linear',
          position: 'bottom'
        }]
      }
  }
  render() {
    return (
      <div>
        <div className="page-header">
          <h3 className="page-title"> Overview </h3>
          <button type="button" className="btn btn-outline-dark btn-gradient-dark btn-rounded btn-fw">Track Parcels</button>
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li className="breadcrumb-item"><a href="!#" onClick={event => event.preventDefault()}>Fulfillment</a></li>
              <li className="breadcrumb-item active" aria-current="page">Overview</li>
            </ol>
          </nav>
        </div>
        <div className="row">
        <div className="col-md-3 stretch-card grid-margin">
            <div className="card bg-gradient-danger card-img-holder text-black">
              <div className="card-body">
                {/* <img src={require("../../assets/images/dashboard/circle.svg")} className="card-img-absolute" alt="circle" /> */}
                <h4 className="font-weight-normal mb-3">Pending <i className="mdi mdi-chart-line mdi-24px float-right"></i>
                </h4>
                <h2 className="mb-5">150</h2>
                <a href="" className="card-text">View Report</a>
              </div>
            </div>
          </div>
          <div className="col-md-3 stretch-card grid-margin">
            <div className="card bg-gradient-danger card-img-holder text-black">
              <div className="card-body">
                {/* <img src={require("../../assets/images/dashboard/circle.svg")} className="card-img-absolute" alt="circle" /> */}
                <h4 className="font-weight-normal mb-3">In-transit <i className="mdi mdi-chart-line mdi-24px float-right"></i>
                </h4>
                <h2 className="mb-5">50</h2>
                <a href="" className="card-text">View Report</a>
              </div>
            </div>
          </div>
          <div className="col-md-3 stretch-card grid-margin">
            <div className="card bg-gradient-info card-img-holder text-black">
              <div className="card-body">
                {/* <img src={require("../../assets/images/dashboard/circle.svg")} className="card-img-absolute" alt="circle" /> */}
                <h4 className="font-weight-normal mb-3">Delivered <i className="mdi mdi-bookmark-outline mdi-24px float-right"></i>
                </h4>
                <h2 className="mb-5">350</h2>
                <a href="" className="card-text">View Report</a>
              </div>
            </div>
          </div>
          <div className="col-md-3 stretch-card grid-margin">
            <div className="card bg-gradient-success card-img-holder text-black">
              <div className="card-body">
                {/* <img src={require("../../assets/images/dashboard/circle.svg")} className="card-img-absolute" alt="circle" /> */}
                <h4 className="font-weight-normal mb-3">Canceled <i className="mdi mdi-diamond mdi-24px float-right"></i>
                </h4>
                <h2 className="mb-5">10</h2>
                <a href="" className="card-text">View Report</a>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6 grid-margin stretch-card">
            
                <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">Parcels Sent</h4>
                                <Bar data={this.data} options={this.options} />    
                            </div>
                        </div>
        
          </div>
          <div className="col-md-6 grid-margin stretch-card">
          <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">Revenue</h4>
                                <Line data={this.areaData} options={this.areaOptions} />
                            </div>
                        </div>
          </div>
          <div className='col-12 stretch-card'>
                            <div className="card">
                              <div className="card-body">
                                <div className="d-xl-flex justify-content-between mb-2">
                                  <h4 className="card-title">Parcel Delivery Analytics</h4>
                                  <div id="visit-sale-chart-legend" className="legend-top-right float-right">
                                    <ul className="legend-horizontal">
                                      <li>
                                        <span className="legend-dots bg-success">
                                        </span>Delivered
                                      </li>
                                      <li>
                                        <span className="legend-dots bg-danger">
                                        </span>Canceled
                                      </li>
                                    </ul>
                                  </div>
                                </div>
                                <Line data={this.pageViewAnalyticData} options={this.pageViewAnalyticOptions} />
                              </div>
                            </div>
                         
          </div>
          <div className='col-12 stretch-card'>
          <div className="card">
          <div className="card-body">
                                <h4 className="card-title">Popular Destinations</h4>
                                <Bar data={this.destinationData} options={this.destinationOptions} />    
                            </div>
                        </div>
          </div>
          <div className='col-12 stretch-card'>
                            <div className="card">
                              <div className="card-body">
                                <div className="d-xl-flex justify-content-between mb-2">
                                  <h4 className="card-title">Parcel Delivery Analytics</h4>
                                  <div id="visit-sale-chart-legend" className="legend-top-right float-right">
                                    <ul className="legend-horizontal">
                                      <li>
                                        <span className="legend-dots bg-success">
                                        </span>Value of items
                                      </li>
                                      <li>
                                        <span className="legend-dots bg-danger">
                                        </span>Delivery Cost
                                      </li>
                                    </ul>
                                  </div>
                                </div>
                                <Line data={this.pageViewAnalyticData} options={this.pageViewAnalyticOptions} />
                              </div>
                            </div>
                         
          </div>
          
          
        </div>
      </div>
    )
  }
}

export default BasicElements
