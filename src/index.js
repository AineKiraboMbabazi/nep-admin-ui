import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './app/App';
import "./i18n";
import "./assets/styles/components/chat.css"
// import "./assets/css/styles.css";
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <BrowserRouter basename="/nep-admin">
    <App />
  </BrowserRouter>
, document.getElementById('root'));

serviceWorker.unregister();